แก้ไขตามหัวข้อ 4. ต้องใช้หลักการโปรแกรมเชิงวัตถุพื้นฐาน เช่น ออกแบบคลาสให้สามารถสร้างวัตถุที่มี attribute ต่างกัน ผ่านการใช้ parameter ใน constructor

"ควรจะ passing color และ ชิ้นส่วนต่างๆผ่าน constructor เลย 
ไม่ต้อง if ถ้าต้องการมีสีเพิ่ม ก็ต้องแก้ไข model คือการสร้าง object จะขึ้นอยู่ กับ ตัวแปร type ถ้าต้องการสีอื่นที่ไม่มีใน if ก็ต้องสร้าง
if เพิ่ม และ Cat Wolf Horse มี attribute และ method ที่ทำงานเหมือนกัน ไม่แตกต่างกัน ต่างกันแค่ method initialize()"

------------------------------------------------------------------------------------------------------------
Cat.java Horse.java Wolf.Java 
    - แก้ constructor เอา int type ออกเปลี่ยนเป็น Color, Color เพื่อน set ค่าของ darkColor และ lightColor แทน
    - เพิ่ม darkColors, lightColors เพื่อเก็บสีที่เป็นไปได้ทั้งหมด
    - เพิ่ม getNumberOfType เพื่อทราบว่าสีที่เป็นไปได้ทั้งหมดของสัตว์ชนิดนั้นเป็นเท่าไหร่
    
SelectRiderPageController, GameController
    - ปรับค่าคงที่ให้กลายเป็นตัวแปรเพื่อเพิ่มความยืดหยุ่นเมื่อมีการอัพเดทใดๆ 
    - ปรับการสร้าง object ของ class Horse, Cat, Wolf ตาม Contructor ที่เปลี่ยนไป
    
