package Controller;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class FirstPageController {

    @FXML
    AnchorPane pane;

    @FXML
    void handleButtonChangeToGame(){
        PageChanger.changePage(pane ,getClass().getResource("/UI/selectRiderPage.fxml"));
    }

    @FXML
    void handleButtonChangeToInformationPage(){
        PageChanger.changePage(pane, getClass().getResource("/UI/information.fxml"));
    }
}
