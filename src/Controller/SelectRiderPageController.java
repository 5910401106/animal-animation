package Controller;

import data.beastRider.BeastRider;
import data.beastRider.Cat;
import data.beastRider.Horse;
import data.beastRider.Wolf;
import javafx.fxml.FXML;
import javafx.scene.layout.Pane;

import java.io.IOException;

/**
 * Controller for control selectRiderPage
 */
public class SelectRiderPageController {
    /**
     * Pane for showing rider's sample
     */
    @FXML
    Pane pane;

    /**
     * Sample rider
     */
    private BeastRider beastRider;

    /**
     * Number of rider that player is selecting
     */
    private int nowSelecting;

    /**
     * Maximum type of riders (4 Horse, 4 Wolf, 4 Cat)
     */
    private final int maxType = Horse.getNumberOfType() + Wolf.getNumberOfType() + Cat.getNumberOfType();

    /**
     * Initialize sample rider
     */
    @FXML
    public void initialize(){
        beastRider = new BeastRider(0,0, new Horse(
                Horse.getDarkColor(0),
                Horse.getLightColor(0)
        ));
        beastRider.setScaleX(1.5);
        beastRider.setScaleY(1.5);
        beastRider.setTranslateX(beastRider.getWidth()/2);
        beastRider.setTranslateY(beastRider.getHeight()/2);
        nowSelecting = Setting.getRiderType();
        selectRider();
        pane.getChildren().add(beastRider);
    }

    /**
     * Set pet and rider to show the sample
     */
    private void selectRider(){
        if (nowSelecting%maxType < Horse.getNumberOfType()){
            beastRider.setPetAndRider(new Horse(
                    Horse.getDarkColor(nowSelecting%Horse.getNumberOfType()),
                    Horse.getLightColor(nowSelecting%Horse.getNumberOfType())
            ));
        }
        else if (nowSelecting%maxType < Horse.getNumberOfType() + Cat.getNumberOfType()){
            beastRider.setPetAndRider(new Cat(
                    Cat.getDarkColor(nowSelecting%Cat.getNumberOfType()),
                    Cat.getLightColor(nowSelecting%Cat.getNumberOfType())
            ));
        }
        else {
            beastRider.setPetAndRider(new Wolf(
                    Wolf.getDarkColor(nowSelecting%Wolf.getNumberOfType()),
                    Wolf.getLightColor(nowSelecting%Wolf.getNumberOfType())
            ));
        }
    }

    /**
     * change type of sample rider to next type
     */
    @FXML
    public void handleButtonChangeToNextRider(){
        nowSelecting++;
        if (nowSelecting >= maxType){
            nowSelecting -= maxType;
        }
        selectRider();
    }

    /**
     * change type of sample rider to previous type
     */
    @FXML
    public void handleButtonChangeToPreviousRider(){
        nowSelecting--;
        if (nowSelecting < 0)
            nowSelecting += maxType;
        selectRider();
    }

    /**
     * confirm and go to next page
     */
    @FXML
    public void handleButtonGoToNextPage(){
        Setting.setRiderType(nowSelecting);
        PageChanger.changePage(pane ,getClass().getResource("/UI/setting.fxml"));
    }

    @FXML
    public void handleButtonGoToPreviousPage(){
        Setting.setRiderType(nowSelecting);
        PageChanger.changePage(pane, getClass().getResource("/UI/firstPage.fxml"));
    }
}
