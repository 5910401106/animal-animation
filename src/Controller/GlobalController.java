package Controller;

import data.AnimationOrder;
import data.beastRider.BeastRider;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Controller for control all rider in game
 */
class GlobalController implements BeastRiderController{

    /**
     * All rider in game
     */
    private ArrayList<BeastRider> beastRiders;

    /**
     * Initialize beastRiders
     * @param beastRiders objects of BeastRider that will be store in this class
     */
    GlobalController(BeastRider... beastRiders){
        this.beastRiders = new ArrayList<>();
        this.beastRiders.addAll(Arrays.asList(beastRiders));
    }

    /**
     * Set all rider's AnimationOrder to START
     */
    public void startRunning(){
        for (BeastRider beastRider : beastRiders){
            beastRider.setRunningOrder(AnimationOrder.START);
        }
    }

    /**
     * Set all rider's AnimationOrder to STOP
     */
    public void stopRunning(){
        for(BeastRider beastRider: beastRiders){
            beastRider.setRunningOrder(AnimationOrder.STOP);
        }
    }
}
