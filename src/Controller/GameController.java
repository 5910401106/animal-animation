package Controller;

import data.AnimationOrder;
import data.Map.CompetitiveMap;

import data.beastRider.BeastRider;
import data.beastRider.Cat;
import data.beastRider.Horse;
import data.beastRider.Wolf;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Duration;
import java.util.Random;

/**
 * Controller that control game page
 */
public class GameController {

    /**
     * main pane which show the game(animation)
     */
    @FXML
    protected Pane pane;

    /**
     * label that show what key you should press such as "press 'space' to start" , "press '[A-Z]' to increase Speed"
     */
    @FXML
    protected Label label;

    /**
     * field for calculate position of rider in game
     */
    private final int maxRunField = 270;
    /**
     * number of rider refer by Setting
     */
    private int numberOfRider;
    /**
     * number of Player position in game sequence from up to down
     */
    private int playerNumber;
    /**
     * GameCharacters
     */
    private BeastRider[] beastRiders;
    /**
     * Map background
     */
    private CompetitiveMap competitiveMap;
    /**
     * All riders controller
     */
    private GlobalController globalController;
    /**
     * Controller that control only Player's rider
     */
    private PlayerController playerController;

    /**
     * Keyframe for animate competition
     */
    private KeyFrame runningKeyFrame;
    /**
     * Timeline for animate competition
     */
    private Timeline animationTimeline;
    /**
     * Checking Is game already start
     */
    private boolean isRunning;

    /**
     * Alphabet that is a key that press to increase speed
     */
    private char increaseSpeedKey;


    /**
     * Initialize all game's objects
     * Set player's rider
     * Initialize key controller
     * Set globalController, playerController
     * Set timeline
     */
    @FXML
    void initialize(){
        numberOfRider = Setting.getNumberOfRiders();
        playerNumber = new Random().nextInt(Setting.getNumberOfRiders());
        isRunning = false;
        initializeGameObject();
        initialPlayerRider();
        initialKeyController();
        pane.getChildren().remove(label);
        pane.getChildren().add(competitiveMap);
        pane.getChildren().addAll(beastRiders);
        pane.getChildren().add(label);

        globalController = new GlobalController(beastRiders);
        playerController = new PlayerController(beastRiders[playerNumber]);
        animationTimeline = new Timeline();
        animationTimeline.setCycleCount(Timeline.INDEFINITE);
    }

    /**
     * Initialize all riders and map
     */
    private void initializeGameObject(){
        int numberOfAllType = Horse.getNumberOfType() + Cat.getNumberOfType() + Wolf.getNumberOfType();
        beastRiders = new BeastRider[numberOfRider];
        for (int i = 0; i < numberOfRider; i++){
            int type = new Random().nextInt(100);
            if (type % numberOfAllType >= Cat.getNumberOfType() + Wolf.getNumberOfType()){
                beastRiders[i] = new BeastRider(10,210 + i * maxRunField/numberOfRider, new Horse(
                        Horse.getDarkColor(type%Horse.getNumberOfType()),
                        Horse.getLightColor(type%Horse.getNumberOfType())
                ));
            }
            else if (type % numberOfAllType > Wolf.getNumberOfType()){
                beastRiders[i] = new BeastRider(10,210 + i * maxRunField/numberOfRider, new Cat(
                        Cat.getDarkColor(type%Cat.getNumberOfType()),
                        Cat.getLightColor(type%Cat.getNumberOfType())
                ));
            }
            else {
                beastRiders[i] = new BeastRider(10, 210 + i * maxRunField/numberOfRider, new Wolf(
                        Wolf.getDarkColor(type%Wolf.getNumberOfType()),
                        Wolf.getLightColor(type%Wolf.getNumberOfType())
                ));
            }
            if (i != playerNumber)
                beastRiders[i].setColorAndReDrawRider(Color.grayRgb(38));
        }

        competitiveMap = new CompetitiveMap(
                Setting.getMap(),
                Setting.getMapDistance()
        );
    }

    /**
     * Initialize player's rider
     */
    private void initialPlayerRider(){
        int type = Setting.getRiderType();
        if (type >= 8)
            beastRiders[playerNumber] = new BeastRider(10, 210+playerNumber*maxRunField/numberOfRider, new Wolf(
                    Wolf.getDarkColor(type%Wolf.getNumberOfType()),
                    Wolf.getLightColor(type%Wolf.getNumberOfType())
            ));
        else if (type >= 4)
            beastRiders[playerNumber] = new BeastRider(10, 210+playerNumber*maxRunField/numberOfRider, new Cat(
                    Cat.getDarkColor(type%Cat.getNumberOfType()),
                    Cat.getLightColor(type%Cat.getNumberOfType())
            ));
        else
            beastRiders[playerNumber] = new BeastRider(10, 210+playerNumber*maxRunField/numberOfRider, new Horse(
                    Horse.getDarkColor(type%Horse.getNumberOfType()),
                    Horse.getLightColor(type%Horse.getNumberOfType())
            ));
    }

    /**
     * Initialize and set key control
     */
    private void initialKeyController(){
        increaseSpeedKey = randomChar();
        pane.setFocusTraversable(true);
        pane.getParent().setOnKeyPressed(event -> {
            if (!isRunning){
                if (event.getCode().equals(KeyCode.SPACE)) {
                    startRunning();
                    isRunning = true;
                }
            }
            else if (event.getCode().getName().equalsIgnoreCase(String.valueOf(increaseSpeedKey))){
                changeSpeed(1);
                increaseSpeedKey = randomChar();
                label.setText("Press '" + String.valueOf(increaseSpeedKey) + "' to increase speed");
            }
            else if (event.getText().length() > 0) {
                if (Character.isLetter(event.getText().charAt(0))) {
                    changeSpeed(-3);
                    increaseSpeedKey = randomChar();
                    label.setText("Press '" + String.valueOf(increaseSpeedKey) + "' to increase speed");
                }
            }
        });
    }

    /**
     * Using for random key that use to increase speed
     * @return random character
     */
    private char randomChar(){
        return (char)(new Random().nextInt(26) + 'A');
    }

    /**
     * Animate rider to running
     * AnimationOrder.START -> rider run
     * AnimationOrder.STOP -> rider stop
     */
    private void animateRunning(){
        animationTimeline.stop();
        animationTimeline.getKeyFrames().remove(runningKeyFrame);
        runningKeyFrame = new KeyFrame(
                Duration.millis(20),
                new EventHandler<ActionEvent>() {
                    boolean isAnyoneRunning;
                    @Override
                    public void handle(ActionEvent event) {
                        isAnyoneRunning = false;
                        for (BeastRider beastRider : beastRiders){
                            beastRider.finishCompetition(competitiveMap);
                            double dSpeed = -competitiveMap.getMapSpeed();
                            switch (beastRider.getRunningOrder()){
                                case STOP:
                                    beastRider.stoppingPet();
                                    if (playerController.isPlayer(beastRider)) {
                                        if (competitiveMap.getMapSpeed() != 0)
                                            restartPopUp();
                                        label.setVisible(false);
                                        competitiveMap.setMapSpeed(0);
                                    }
                                    else
                                        beastRider.moveForward(dSpeed);
                                    break;
                                case START:
                                    isAnyoneRunning = true;
                                    beastRider.runningMotion();
                                    dSpeed += beastRider.getRunSpeed();
                                    beastRider.moveForward(dSpeed);
                                    if (playerController.isPlayer(beastRider)) {
                                        competitiveMap.moveForward();
                                    }
                            }
                        }
                        if (!isAnyoneRunning){
                            animationTimeline.stop();
                            animationTimeline.getKeyFrames().remove(runningKeyFrame);
                            if (!animationTimeline.getKeyFrames().isEmpty())
                                animationTimeline.play();
                        }
                    }
                }
        );
        animationTimeline.getKeyFrames().add(runningKeyFrame);
        animationTimeline.play();
    }

    /**
     * Start a running competition
     */
    private void startRunning(){
        label.setText("Press '" + String.valueOf(increaseSpeedKey) + "' to increase speed");
        globalController.startRunning();
        competitiveMap.setMapSpeed(playerController.getRunSpeed());
        animateRunning();
    }

    /**
     * changing player's running speed if player's AnimationOrder is not STOP
     * @param dSpeed changing speed
     */
    private void changeSpeed(double dSpeed){
        if (playerController.getAnimationOrder() == AnimationOrder.STOP)
            return;
        playerController.changeSpeed(dSpeed);
        competitiveMap.setMapSpeed(playerController.getRunSpeed());
    }

    /**
     * Popup restart button when player complete the competition
     */
    private void restartPopUp(){
        Button button = new Button("RESTART");
        button.setFont(new Font(20));
        button.setTranslateX(400);
        button.setTranslateY(300);
        button.setPrefWidth(300);
        button.setPrefHeight(100);
        pane.getChildren().add(button);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                PageChanger.changePage(pane, getClass().getResource("/UI/selectRiderPage.fxml"));
            }
        });
    }
}
