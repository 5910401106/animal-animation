package Controller;

import data.Map.*;

/**
 * Class that store Game's Settings
 */
public class Setting {

    /**
     * Player's rider type
     */
    private static int riderType = 0;

    /**
     * Number of riders in game
     */
    private static int numberOfRiders = 2;

    /**
     * Distance of Map (Number of parts not number of pixels)
     */
    private static int mapDistance = 50;

    /**
     * Type of Map
     */
    private static int mapType = 0;

    private static final MapPart[] mapParts = new MapPart[]{
            new MeadowMapPart(0,0),
            new CastleMapPart(0,0),
            new ForestMapPart(0,0),
            new WarFieldMapPart(0,0)
    };

    public static int getRiderType() {
        return riderType;
    }

    public static void setRiderType(int riderType) {
        Setting.riderType = riderType;
    }

    public static int getNumberOfRiders() {
        return numberOfRiders;
    }

    public static void setNumberOfRiders(int numberOfRiders) {
        Setting.numberOfRiders = numberOfRiders;
    }

    public static int getMapDistance() {
        return mapDistance;
    }

    public static void setMapDistance(int mapDistance) {
        Setting.mapDistance = mapDistance;
    }

    public static int getMapType() {
        return mapType;
    }

    public static void setMapType(int mapType) {
        Setting.mapType = mapType;
    }

    public static MapPart getMap(){
        return mapParts[mapType];
    }
}
