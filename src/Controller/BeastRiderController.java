package Controller;

/**
 * Controller that control beast rider
 */
interface BeastRiderController {
    void startRunning();
    void stopRunning();
}
