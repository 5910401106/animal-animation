package Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;

/**
 * Controller that control Setting page
 */
public class SettingController {

    /**
     * Choice box for choosing number of riders
     */
    @FXML
    ChoiceBox<Integer> numberOfRiders;

    /**
     * Choice box for choosing map's distance
     */
    @FXML
    ChoiceBox<Integer> mapDistance;

    /**
     * Choice box for choosing map
     */
    @FXML
    ChoiceBox<String> mapSelection;

    /**
     * Name of maps
     */
    private String[] mapType = new String[]{
         "Meadow Map",
            "Castle Map",
            "Forest Map",
            "War Field Map"
    };

    /**
     * Initialize all choice box and set default by 'Setting'
     */
    @FXML
    public void initialize(){
        numberOfRiders.getItems().addAll(2,3,4,5,6,7,8,9);
        numberOfRiders.setValue(Setting.getNumberOfRiders());
        mapDistance.getItems().addAll(50,150,250,350,500,750,1000,1250,1500,2000,4000);
        mapDistance.setValue(Setting.getMapDistance());
        mapSelection.getItems().addAll(mapType);
        mapSelection.setValue(mapType[Setting.getMapType()]);
    }

    /**
     * Go to game page
     */
    @FXML
    public void handleButtonStart() {
        confirmSetting();
        PageChanger.changePage(numberOfRiders,getClass().getResource("/UI/game.fxml"));
    }

    /**
     * Back to SelectRiderPage
     */
    @FXML
    public void handleButtonBack() {
        confirmSetting();
        PageChanger.changePage(numberOfRiders,getClass().getResource("/UI/selectRiderPage.fxml"));
    }

    /**
     * set data to 'Setting'
     */
    private void confirmSetting(){
        Setting.setNumberOfRiders(numberOfRiders.getValue());
        Setting.setMapDistance(mapDistance.getValue());
        Setting.setMapType(Arrays.asList(mapType).indexOf(mapSelection.getValue()));
    }
}
