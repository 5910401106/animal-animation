package Controller;

import data.AnimationOrder;
import data.beastRider.BeastRider;
import javafx.scene.paint.Color;

/**
 * Controller for control only player
 */
class PlayerController implements BeastRiderController{
    /**
     * Player's rider
     */
    BeastRider player;

    /**
     * Initialize Rider
     * Set player's running speed
     * Set player's color
     * @param player the object of BeastRider that will be store in this class
     */
    PlayerController(BeastRider player) {
        player.setRunSpeed(3);
        this.player = player;
        player.setColorAndReDrawRider(Color.rgb(255, 188, 146));
    }

    /**
     * set player's running speed to ('present speed' + 'dSpeed')
     * @param dSpeed changing speed
     */
    void changeSpeed(double dSpeed){
        if (player.getRunningOrder() == AnimationOrder.STOP){
            return;
        }
        player.setRunSpeed(player.getRunSpeed()+dSpeed);
    }

    /**
     * get player's running speed
     * @return player's running speed
     */
    double getRunSpeed(){
        return player.getRunSpeed();
    }

    /**
     * set player's AnimationOrder to STOP
     */
    public void stopRunning(){
        player.setRunningOrder(AnimationOrder.STOP);
    }

    /**
     * set player's AnimationOrder to START
     */
    public void startRunning(){
        player.setRunningOrder(AnimationOrder.START);
    }

    /**
     * check is beastRider player?
     * @param beastRider the object that get for checking is it player object
     * @return is beastRider player
     */
    boolean isPlayer(BeastRider beastRider){
        return beastRider.equals(player);
    }

    /**
     * get player's AnimationOrder
     * @return player's AnimationOrder
     */
    AnimationOrder getAnimationOrder(){
        return player.getRunningOrder();
    }
}
