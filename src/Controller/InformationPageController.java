package Controller;

import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;

public class InformationPageController {
    @FXML
    GridPane pane;

    @FXML
    void handleButtonGoToPreviousPage(){
        PageChanger.changePage(pane, getClass().getResource("/UI/firstPage.fxml"));
    }
}
