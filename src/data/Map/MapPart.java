package data.Map;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * One part of full map
 */
public abstract class MapPart extends Canvas {
    /**
     * Color of sky
     */
    private static Color skyColor = Color.rgb(144, 177, 205);
    /**
     * Color of midday
     */
    private Color dayColor = Color.rgb(173, 193, 205);
    /**
     * Color of midnight
     */
    private Color nightColor = Color.rgb(0, 8, 31);


    /**
     * Initialize object and draw
     * @param x position in X Axis
     * @param y position in Y Axis
     */
    public MapPart(double x, double y){
        setTranslateX(x);
        setTranslateY(y);
        setWidth(200);
        setHeight(600);
    }

    abstract MapPart newPart(double x, double y);

    /**
     * moving opposite side of rider with 'speed'
     * @param speed the changing position
     */
    public void moveForward(double speed){
        setTranslateX(getTranslateX()-speed);
    }

    void draw(){
        GraphicsContext gc = getGraphicsContext2D();
        gc.clearRect(0,0,getWidth(),getHeight());
        gc.setFill(skyColor);
        gc.fillRect(0,0,200,600);
    }

    /**
     * drawing the start line/end line
     */
    void drawStartEndLine(){
        GraphicsContext gc = getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(150, 301, 50, 299);
        gc.setFill(Color.BLACK);
        gc.fillRect(150,301,25,25);
        gc.fillRect(150, 351, 25, 25);
        gc.fillRect(150, 401, 25, 25);
        gc.fillRect(150, 451, 25, 25);
        gc.fillRect(150, 501, 25, 25);
        gc.fillRect(150, 551, 25, 25);
        for (int i = 0; i < 6; i++){
            gc.fillRect(150, 301+50*i, 25, 25);
            gc.fillRect(175, 325+50*i, 25,25);
        }
    }

    /**
     * Changing sky color
     * @param goDark the boolean that show now is day or night
     * @return not goDark if sky is completely bright/dark
     */
    boolean changeSkyColor(boolean goDark){
        if (goDark) {
            skyColor =  skyColor.interpolate(nightColor, 0.013);
            if (skyColor.getRed() < 0.01 && skyColor.getGreen() < 0.04 && skyColor.getBlue() < 0.13)
                goDark = false;
        }
        else{
            skyColor =  skyColor.interpolate(dayColor, 0.013);
            if (skyColor.getRed() > 0.67 && skyColor.getGreen() > 0.75 && skyColor.getBlue() > 0.79)
                goDark = true;
        }
        draw();
        return goDark;
    }

}
