package data.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Map part of Forest Map
 */
public class ForestMapPart extends MapPart {
    /**
     * Initialize object and draw
     *
     * @param x position in X Axis
     * @param y position in Y Axis
     */
    public ForestMapPart(double x, double y) {
        super(x, y);
    }

    /**
     * Get new meadow map part in position (x,y)
     * @param x position in X Axis
     * @param y position in Y Axis
     * @return new Forest map part
     */
    @Override
    MapPart newPart(double x, double y) {
        return new ForestMapPart(x,y);
    }

    /**
     * Drawing object
     */
    @Override
    void draw() {
        super.draw();
        GraphicsContext gc = getGraphicsContext2D();
        gc.setLineWidth(2);
        double[] treeLeaf1X = new double[]{0,24,61,55,85,98,80,86,88,59,35,0};
        double[] treeLeaf2X = new double[]{200,176,139,146,115,104,121,114,112,141,164,200};
        double[] treeLeafY = new double[]{55,44,67,77,75,142,141,147,183,194,183,183};
        double[] treeTrunk1X = new double[]{0,13,40,29,44,0};
        double[] treeTrunk2X = new double[]{200,189,163,170,156,200};
        double[] treeTrunkY = new double[]{133,129,153,220,301,301};
        gc.setFill(Color.rgb(27, 209, 18));
        gc.fillPolygon(treeLeaf1X,treeLeafY,treeLeaf1X.length);
        gc.fillPolygon(treeLeaf2X,treeLeafY,treeLeaf2X.length);
        gc.setStroke(Color.rgb(0, 150, 2));
        gc.strokePolyline(treeLeaf1X,treeLeafY,treeLeaf1X.length);
        gc.strokePolyline(treeLeaf2X,treeLeafY,treeLeaf2X.length);
        gc.setFill(Color.rgb(150, 62, 44));
        gc.fillPolygon(treeTrunk1X, treeTrunkY, treeTrunk1X.length);
        gc.fillPolygon(treeTrunk2X, treeTrunkY, treeTrunk2X.length);
        gc.setStroke(Color.rgb(102, 23, 9));
        gc.strokePolyline(treeTrunk1X, treeTrunkY, treeTrunk1X.length);
        gc.strokePolyline(treeTrunk2X, treeTrunkY, treeTrunk2X.length);
        gc.setFill(Color.rgb(214, 111, 74));
        gc.fillRect(0,301,200,299);
        gc.strokeLine(0,301,200,301);
    }
}
