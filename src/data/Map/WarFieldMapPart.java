package data.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class WarFieldMapPart extends MapPart {
    /**
     * Initialize object and draw
     *
     * @param x position in X Axis
     * @param y position in Y Axis
     */
    public WarFieldMapPart(double x, double y) {
        super(x, y);
    }

    /**
     * Get new meadow map part in position (x,y)
     * @param x position in X Axis
     * @param y position in Y Axis
     * @return new Meadow map part
     */
    @Override
    MapPart newPart(double x, double y) {
        return new WarFieldMapPart(x,y);
    }

    @Override
    void draw(){
        super.draw();
        GraphicsContext gc = getGraphicsContext2D();
        gc.setLineWidth(4);
        gc.setFill(Color.grayRgb(51));
        gc.fillRect(0,0,200,400);
        gc.setFill(Color.rgb(173, 14, 22));
        gc.fillRect(0,250,200,350);
        gc.setStroke(Color.rgb(136, 0, 21));
        gc.strokeLine(0,250,200,250);
        gc.setStroke(Color.BLACK);
        gc.strokeLine(15,97,27,284);
        gc.strokeLine(0,150,36,143);
        gc.strokeLine(49,145,86,302);
        gc.strokeLine(40,209,83,199);
        gc.strokeLine(124,66,64,264);
        gc.strokeLine(90,100, 135,115);
        gc.strokeLine(183,135,109,277);
        gc.strokeLine(140,168,179,194);
        gc.strokeLine(166,204,182,298);
        gc.strokeLine(153,233, 190, 224);
    }

    @Override
    void drawStartEndLine() {
        super.drawStartEndLine();
        GraphicsContext gc = getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(150,251,50,50);
        gc.setFill(Color.BLACK);
        gc.fillRect(150,251,25,25);
        gc.fillRect(175,276,25,25);
    }
}
