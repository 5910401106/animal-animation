package data.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Map part of meadow
 */
public class MeadowMapPart extends MapPart{

    /**
     * Initialize object and draw
     *
     * @param x position in X Axis
     * @param y position in Y Axis
     */
    public MeadowMapPart(double x, double y) {
        super(x, y);
    }


    /**
     * Get new meadow map part in position (x,y)
     * @param x position in X Axis
     * @param y position in Y Axis
     * @return new Meadow map part
     */
    @Override
    MapPart newPart(double x, double y) {
        return new MeadowMapPart(x,y);
    }

    /**
     * Drawing object
     */
    @Override
    void draw(){
        super.draw();
        GraphicsContext gc = getGraphicsContext2D();
        double[] mountainX = new double[]{0,43,100,38,0};
        double[] mountainY = new double[]{58,74,168,301,301};
        double[] mountainX2 = new double[]{200,38,116,184,200};
        double[] mountainY2 = new double[]{301,301,134,62,58};
        gc.setStroke(Color.rgb(130, 30, 18));
        gc.strokePolyline(mountainX,mountainY,mountainX.length);
        gc.strokePolyline(mountainX2,mountainY2,mountainX2.length);
        gc.setFill(Color.rgb(164, 83, 50));
        gc.fillPolygon(mountainX,mountainY,mountainX.length);
        gc.fillPolygon(mountainX2,mountainY2,mountainX2.length);
        gc.setFill(Color.rgb(35, 252, 75));
        gc.fillRect(0,301,200,299);
    }



}
