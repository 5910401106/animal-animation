package data.Map;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * Map part of Castle map
 */
public class CastleMapPart extends MapPart {
    /**
     * Initialize object and draw
     *
     * @param x position in X Axis
     * @param y position in Y Axis
     */
    public CastleMapPart(double x, double y) {
        super(x, y);
    }

    /**
     * Get new meadow map part in position (x,y)
     * @param x position in X Axis
     * @param y position in Y Axis
     * @return new Castle map part
     */
    @Override
    MapPart newPart(double x, double y) {
        return new CastleMapPart(x,y);
    }

    /**
     * Drawing object
     */
    @Override
    void draw() {
        super.draw();
        GraphicsContext gc = getGraphicsContext2D();
        gc.setLineWidth(2);
        double[] wallFillX = new double[]{0,0,50,50,150,150,200,200};
        double[] wallFillY = new double[]{301,220,220,250,250,220,220,301};
        double[] wallStrokeX = new double[]{0,50,50,150,150,200};
        double[] wallStrokeY = new double[]{220,220,250,250,220,220};
        gc.setFill(Color.grayRgb(95));
        gc.fillPolygon(wallFillX, wallFillY, wallFillX.length);
        gc.setFill(Color.grayRgb(157));
        gc.fillRect(0,301,200,299);
        gc.setStroke(Color.grayRgb(50));
        gc.strokePolyline(wallStrokeX, wallStrokeY, wallStrokeX.length);
        gc.strokeLine(0,301,200,301);
    }
}
