package data.Map;

import javafx.scene.layout.Pane;

/**
 * Map that use for running competition and be connected with some of map parts
 */
public class CompetitiveMap extends Pane {
    /**
     * number of map parts
     */
    private final int nMapParts = 6;

    /**
     * some of map parts
     */
    private MapPart[] mapParts;

    /**
     * boolean that use for controlling the sky
     * is now day or night
     */
    private boolean goDark;

    /**
     * map moving speed
     */
    private double mapSpeed;

    /**
     * maximum number of map parts
     */
    private int maxMapPartsDistance;

    /**
     * map parts counter that count by part which passing over the scene edge
     */
    private int mapPartsCounter = 0;

    /**
     * Initialize Object
     * Set map parts
     * Set some of variable
     * Drawing Start line at first map part
     * @param mapPart the object that use for set the type of map part
     * @param mapDistance maximum distance of map part
     */
    public CompetitiveMap(MapPart mapPart, int mapDistance){
        maxMapPartsDistance = mapDistance;
        setTranslateX(0);
        setTranslateY(0);
        setWidth(800);
        setHeight(600);

        mapParts = new MapPart[nMapParts];
        goDark = true;
        mapSpeed = 5;

        for (int i = 0; i < nMapParts; i++){
            mapParts[i] = mapPart.newPart(
                    i*mapPart.getWidth(),
                    0
            );
            mapParts[i].draw();
            getChildren().add(mapParts[i]);
        }

        mapParts[0].drawStartEndLine();
    }

    /**
     * All map parts move opposite side with player rider
     */
    public void moveForward(){
        double dDistance = mapSpeed;
        for (int i = 0; i < nMapParts; i++){
            mapParts[i].moveForward(dDistance);
            if (mapParts[i].getTranslateX()+mapParts[i].getWidth() < 0){
                mapParts[i].setTranslateX(mapParts[i].getTranslateX()+mapParts[i].getWidth()*(nMapParts));
                goDark = mapParts[i].changeSkyColor(goDark);
                mapPartsCounter++;
                if (mapPartsCounter == maxMapPartsDistance-nMapParts+1)
                    mapParts[i].drawStartEndLine();
            }
        }
    }

    /**
     * Get map speed
     * @return map speed
     */
    public double getMapSpeed(){
        return mapSpeed;
    }

    /**
     * Set map speed
     * @param mapSpeed new map speed that use for set map speed
     */
    public void setMapSpeed(double mapSpeed) {
        this.mapSpeed = mapSpeed;
    }

    /**
     * get map's pixel distance
     * @return map part's width multiply with maximum number of map parts
     */
    public double getMapPixelDistance(){
        return mapParts[0].getWidth()*maxMapPartsDistance;
    }
}
