package data.beastRider;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * One type of rideable animal
 */
public class Wolf extends RideableAnimal{

    /**
     * all wolf's color that can be possible
     */
    private static Color[] darkColors = new Color[]{
                    Color.grayRgb(46),
                    Color.rgb(80, 0, 1),
                    Color.rgb(255, 173, 0),
                    Color.rgb(225, 230, 221)
            };
    private static Color[] lightColors = new Color[]{
            Color.grayRgb(168),
            Color.rgb(255, 87, 75),
            Color.rgb(255, 233, 116),
            Color.rgb(255, 251, 238)
    };
    public static Color getDarkColor(int index){
        return darkColors[index];
    }
    public static Color getLightColor(int index){
        return lightColors[index];
    }
    public static int getNumberOfType(){
        return darkColors.length;
    }

    /**
     * Color of Object
     */
    private Color darkColor, lightColor;

    /**
     * Initialize Object, set color and draw
     * @param darkColor, lightColor use for set it's color
     */
    public Wolf(Color darkColor, Color lightColor){
        this.darkColor = darkColor;
        this.lightColor = lightColor;
        initialLeg();
        getChildren().addAll(frontLeg1, frontLeg2, body, backLeg1, backLeg2);
        draw();
    }

    /**
     * Initialize leg and set position of each leg's point
     */
    private void initialLeg(){
        double[] frontLegX = new double[]{11, 0, 11, 36, 33, 22, 16, 29};
        double[] frontLegY = new double[]{44, 63, 83, 84, 75, 72, 63, 42};
        double[] frontLegX2 = new double[]{17, 0, 19, 56, 55, 45, 37, 23, 38};
        double[] frontLegY2 = new double[]{61, 85, 114, 118, 110, 108, 108, 86, 59};

        double[] backLegX = new double[]{18, 0, 17, 30, 61, 59, 51, 36, 28, 38};
        double[] backLegY = new double[]{63, 91, 114, 126, 126, 118, 115, 114, 94, 83};
        double[] backLegX2 = new double[]{34, 0, 36, 69, 66, 61, 42, 26, 62};
        double[] backLegY2 = new double[]{70, 111, 140, 138, 132, 130, 127, 106, 86};

        frontLeg1 = new AnimalLeg(154, 91, 36, 84, frontLegX, frontLegY);
        frontLeg2 = new AnimalLeg(167, 73, 56, 118, frontLegX2, frontLegY2);
        backLeg1 = new AnimalLeg(56, 46, 61, 126, backLegX, backLegY);
        backLeg2 = new AnimalLeg(47, 56, 69, 140, backLegX2, backLegY2);
        frontLeg1.invertDAngle();
        backLeg1.invertDAngle();
    }

    /**
     * drawing object coloring by darkColor and lightColor
     */
    @Override
    protected void draw(){
        body.getGraphicsContext2D().setStroke(Color.grayRgb(46));
        drawMouth();
        drawLeg();
        drawTail();
        drawHeadAndBody();
    }

    private void drawHeadAndBody() {
        GraphicsContext gc = body.getGraphicsContext2D();
        double[] x1 = new double[]{256,248,234,228,207,200,177,162,173,194};
        double[] y1 = new double[]{80,65,56,6,32,59,71,74,79,77};
        double[] x2 = new double[]{172,163,169,181};
        double[] y2 = new double[]{80,84,89,92};
        double[] x3 = new double[]{165,153,85,77,73,72,78,91,128,183,201,207,222};
        double[] y3 = new double[]{87,92,94,93,103,127,139,144,145,139,138,129,123};
        double[] darkColortX = new double[]{256,248,234,228,207,200,177,162,172,163,165,153,85,77,73,72,78,91,128,183,201,207,222};
        double[] darkColorY = new double[]{80,65,56,6,32,59,71,74,80,84,87,92,94,93,103,127,139,144,145,139,138,129,123};
        double[] lightColorX = new double[]{116,128,183,201,207,222,257,230,210,180,145};
        double[] lightColorY = new double[]{147,145,139,138,129,123,82,93,94,114,130};
        gc.setFill(darkColor);
        gc.fillPolygon(darkColortX, darkColorY, darkColortX.length);
        gc.setFill(lightColor);
        gc.fillPolygon(lightColorX, lightColorY, lightColorX.length);
        gc.setLineWidth(2);
        gc.strokePolyline(x1,y1,x1.length);
        gc.strokePolyline(x2,y2,x2.length);
        gc.strokePolyline(x3,y3,x3.length);
        gc.setStroke(Color.rgb(255, 210, 42));
        gc.setLineWidth(3);
        gc.setFill(Color.rgb(255, 150, 112));
        gc.fillOval(220,61,16,23);
        gc.strokeOval(220,61,16,23);
    }

    private void drawMouth(){
        GraphicsContext gc = body.getGraphicsContext2D();
        gc.setLineWidth(2);
        double[] upperMouseX = new double[]{256,282,292,294,287,278,270,264,251,238,231,221,249};
        double[] upperMouseY = new double[]{80,86,87,96,103,97,105,99,108,99,106,100,73};
        double[] lowerMouseX = new double[]{218,230,259,283,291,278,270,264,251,238,231};
        double[] lowerMouseY = new double[]{118,123,119,112,103,97,105,99,108,99,106};
        gc.setStroke(darkColor);
        gc.setFill(lightColor);
        gc.fillPolygon(upperMouseX, upperMouseY, upperMouseX.length);
        gc.fillPolygon(lowerMouseX, lowerMouseY, lowerMouseX.length);
        gc.strokePolygon(upperMouseX, upperMouseY, upperMouseX.length);
        gc.strokePolygon(lowerMouseX, lowerMouseY, lowerMouseX.length);
    }

    private void drawLeg(){
        frontLeg1.draw(darkColor, lightColor);
        frontLeg2.draw(darkColor, lightColor);
        backLeg1.draw(darkColor, darkColor);
        backLeg2.draw(darkColor, darkColor);
    }

    private void drawTail(){
        double[] x = new double[]{77,64,47,44,37,32,33,40,30,26,34,50,33,40,55,76};
        double[] y = new double[]{102,75,19,3,10,30,36,53,36,53,70,77,73,85,95,105};

        GraphicsContext gc = body.getGraphicsContext2D();
        gc.setFill(darkColor);
        gc.fillPolygon(x,y,x.length);
        gc.strokePolyline(x,y,x.length);
    }



}
