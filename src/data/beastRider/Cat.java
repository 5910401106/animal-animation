package data.beastRider;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

public class Cat extends RideableAnimal {
    /**
     * all horse's color that can be possible
     */
    private static Color[] darkColors = new Color[]{
            Color.grayRgb(40),
            Color.rgb(168, 179, 255),
            Color.rgb(255, 111, 0),
            Color.rgb(75, 31, 22)
    };
    private static Color[] lightColors = new Color[]{
            Color.rgb(72, 71, 67),
            Color.WHITE,
            Color.rgb(255, 207, 97),
            Color.rgb(214, 111, 74)
    };
    public static Color getDarkColor(int index){
        return darkColors[index];
    }
    public static Color getLightColor(int index){
        return lightColors[index];
    }
    public static int getNumberOfType(){
        return darkColors.length;
    }

    /**
     * Color of Object
     */
    private Color darkColor, lightColor;

    /**
     * Initialize Object, set color and draw
     * @param darkColor, lightColor use for set it's color
     */
    public Cat(Color darkColor, Color lightColor){
        this.darkColor = darkColor;
        this.lightColor = lightColor;
        initializeLeg();
        getChildren().addAll(frontLeg1,frontLeg2,backLeg1,backLeg2,body);
        draw();
    }

    /**
     * Initialize leg and set position of each leg's point
     */
    private void initializeLeg() {
        double[] frontLegX = new double[]{2,8,16,20};
        double[] frontLegY = new double[]{18,38,36,13};
        double[] frontLegX2 = new double[]{1,6,14,20};
        double[] frontLegY2 = new double[]{9,32,31,11};
        double[] backLegX = new double[]{2,3,14,20};
        double[] backLegY = new double[]{9,31,32,12};
        double[] backLegX2 = new double[]{2,3,14,21};
        double[] backLegY2 = new double[]{7,31,31,12};

        frontLeg1 = new AnimalLeg(223,150,23,36,frontLegX,frontLegY);
        frontLeg2 = new AnimalLeg(194,161,21,32,frontLegX2,frontLegY2);
        backLeg1 = new AnimalLeg(137,165, 21,34, backLegX, backLegY);
        backLeg2 = new AnimalLeg(104,159, 22,35, backLegX2, backLegY2);
        frontLeg1.invertDAngle();
        frontLeg2.invertDAngle();
    }

    /**
     * drawing object coloring by darkColor and lightColor
     */
    @Override
    void draw() {
        drawLeg();
        drawBody();
    }

    private void drawBody(){
        GraphicsContext gc = body.getGraphicsContext2D();
        gc.setLineWidth(2);
        double[] wholeX = new double[]{
                174,186,195,201,227,230,239,254,262,261,
                248,228,194,149,119,95,83,80,60,50,
                48,58,75,76,64,61,68,79,81,94,
                122,152
        };
        double[] wholeY = new double[]{
                72,51,50,61,61,49,49,77,107,147,
                165,177,183,185,181,169,151,141,140,128,
                109,100,100,110,112,117,128,127,105,85,
                67,64
        };
        gc.setStroke(darkColor);
        gc.setFill(lightColor);
        gc.fillPolygon(wholeX, wholeY, wholeX.length);
        gc.strokePolygon(wholeX, wholeY, wholeX.length);
        gc.setFill(darkColor);
        gc.fillArc(201,46,7,29,180,180, ArcType.CHORD);
        gc.fillArc(210,46,7,29,180,180, ArcType.CHORD);
        gc.fillArc(219,46,7,29,180,180, ArcType.CHORD);
        gc.fillArc(123,44,13,43,183,177, ArcType.CHORD);
        gc.fillArc(143,43,13,43,180,180, ArcType.CHORD);
        gc.fillOval(191,77,7,9);
        gc.fillOval(231,78,7,9);
        gc.strokeArc(203,80,11,20,230,130, ArcType.OPEN);
        gc.strokeArc(214,80,11,20,180,130, ArcType.OPEN);
        gc.strokeArc(149,91,50,46,70,55, ArcType.OPEN);
        gc.strokeArc(152,97,50,46,80,55, ArcType.OPEN);
        gc.strokeArc(232,91,50,46,70,55, ArcType.OPEN);
        gc.strokeArc(230,97,50,46,60,55, ArcType.OPEN);
    }

    private void drawLeg(){
        frontLeg1.draw(darkColor, lightColor);
        frontLeg2.draw(darkColor, lightColor);
        backLeg1.draw(darkColor, lightColor);
        backLeg2.draw(darkColor, lightColor);
    }
}
