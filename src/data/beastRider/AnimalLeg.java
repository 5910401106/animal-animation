package data.beastRider;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * the canvas of leg that can show with drawing and motion for animate the running movement
 */
class AnimalLeg extends Canvas{
    /**
     * the base angle that use for rotate this object for running motion
     */
    private final double BASEDANGLE = 5;
    /**
     * the position that use for drawing this object to showing it
     */
    private double[] posX, posY;
    /**
     * the angle that use for rotate this object for running motion
     */
    private double dAngle;
    /**
     * the maximize angle that this object's rotation angle should not be over it
     */
    private final double maximizeLegAngle = 50;

    /**
     * Initialize object and set base dAngle
     * @param x position of this object in X Axis
     * @param y position of this object in Y Axis
     * @param width object's width
     * @param height object's height
     * @param posX array of points in X Axis that use for drawing this object
     * @param posY array of points in Y Axis that use for drawing this object
     */
    AnimalLeg(double x, double y, double width, double height, double[] posX, double[] posY) {
        super(width, height);
        setTranslateX(x);
        setTranslateY(y);
        this.posX = posX;
        this.posY = posY;
        dAngle = BASEDANGLE;
    }

    /**
     * invert the value of dAngle
     */
    void invertDAngle(){
        dAngle = -dAngle;
    }

    /**
     * drawing the object
     * @param strokeColor color of line
     * @param fillColor color for filling the object
     */
    void draw(Color strokeColor, Color fillColor){
        GraphicsContext gc = getGraphicsContext2D();
        gc.setLineWidth(2);
        gc.setStroke(strokeColor);
        gc.setFill(fillColor);
        gc.fillPolygon(posX, posY, posX.length);
        gc.strokePolyline(posX, posY, posX.length);
    }

    /**
     * the movement that is the one of several parts of running animation
     */
    void motion(){
        if (getRotate() > maximizeLegAngle || getRotate() < -maximizeLegAngle){
            if (getRotate() > maximizeLegAngle){
                setRotate(maximizeLegAngle);
            }
            else if (getRotate() < -maximizeLegAngle){
                setRotate(-maximizeLegAngle);
            }
            dAngle = -dAngle;
        }
        setRotate(getRotate()+dAngle);
    }

    /**
     * set rotation to zero (stop running just be standing)
     */
    void stopMotion(){
        setRotate(0);
    }

    /**
     * set the value of dAngle that new dAngle's negative/positive is refer by old dAngle
     * if old dAngle is negative, new dAngle will be negative too
     * @param dAngle new dAngle
     */
    void setDAngle(double dAngle){
        if (this.dAngle > 0)
            this.dAngle = dAngle;
        else
            this.dAngle = -dAngle;
    }
}
