package data.beastRider;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 * One type of rideable animal
 */
public class Horse extends RideableAnimal {

    /**
     * all horse's color that can be possible
     */
    private static Color[] darkColors = new Color[]{
            Color.grayRgb(0),
            Color.rgb(46, 14, 0),
            Color.rgb(255, 218, 0),
            Color.grayRgb(140)

    };
    private static Color[] lightColors = new Color[]{
            Color.rgb(72, 71, 67),
            Color.rgb(164, 83, 50),
            Color.rgb(254, 255, 166),
            Color.grayRgb(210)
    };
    public static Color getDarkColor(int index){
        return darkColors[index];
    }
    public static Color getLightColor(int index){
        return lightColors[index];
    }
    public static int getNumberOfType(){
        return darkColors.length;
    }

    /**
     * Color of Object
     */
    private Color darkColor, lightColor;

    /**
     * Initialize Object, set color and draw
     * @param darkColor, lightColor use for set it's color
     */
    public Horse(Color darkColor, Color lightColor){
        this.darkColor = darkColor;
        this.lightColor = lightColor;
        initializeLeg();
        getChildren().addAll(frontLeg1, backLeg1, body, frontLeg2, backLeg2);
        draw();
    }

    /**
     * Initialize leg and set position of each leg's point
     */
    private void initializeLeg(){
        double[] frontLegX = new double[]{0, 4, 17, 26};
        double[] frontLegY = new double[]{55, 99, 100, 50};
        double[] frontLegX2 = new double[]{0, 2, 15, 29, 32};
        double[] frontLegY2 = new double[]{70, 78, 129, 127, 64};
        double[] backLegX = new double[]{17, 15, 3, 0};
        double[] backLegY = new double[]{56, 96, 95, 48};
        double[] backLegX2 = new double[]{0, 4, 20, 21};
        double[] backLegY2 = new double[]{60, 120, 117, 74};

        frontLeg1 = new AnimalLeg(170, 74, 26, 100, frontLegX, frontLegY);
        frontLeg2 = new AnimalLeg(179, 51, 32, 129, frontLegX2, frontLegY2);
        backLeg1 = new AnimalLeg(114, 74, 17, 96, backLegX, backLegY);
        backLeg2 = new AnimalLeg(103, 55, 21, 120, backLegX2, backLegY2);
        frontLeg2.invertDAngle();
        backLeg2.invertDAngle();
    }

    /**
     * drawing object coloring by darkColor and lightColor
     */
    @Override
    protected void draw(){
        body.getGraphicsContext2D().setStroke(darkColor);
        body.getGraphicsContext2D().setLineWidth(2);
        drawHead();
        drawTail();
        drawBody();
    }

    private void drawHead(){
        GraphicsContext gc = body.getGraphicsContext2D();
        double[] headX = new double[]{212,233,227,233,234,250,270,271,245,252,250,231,206,187};
        double[] headY = new double[]{83,60,49,60,68,80,74,51,21,16,1,11,56,70};
        double[] hairX = new double[]{254,263,263,253,235,214,201,192,182,191,205};
        double[] hairY = new double[]{30,25,14,10,5,8,20,40,45,54,57};
        gc.setFill(darkColor);
        gc.fillPolygon(hairX, hairY, hairX.length);
        gc.strokePolyline(hairX, hairY, hairX.length);
        gc.setFill(lightColor);
        gc.fillPolygon(headX, headY, headX.length);
        gc.strokePolyline(headX, headY, headX.length);
//        gc.strokeLine(228,57,213,82);
        gc.strokeLine(231,44,240,63);
        gc.strokeLine(249,79,240,63);
        gc.strokeLine(264,56,266,64);
        gc.setLineWidth(3);
        gc.setFill(Color.rgb(255, 150, 112));
        gc.fillOval(238,32,15,22);
        gc.setStroke(Color.rgb(255, 210, 42));
        gc.strokeOval(238,32,15,22);
        gc.setStroke(darkColor);
        gc.setFill(lightColor);
        gc.setLineWidth(2);
    }

    private void drawBody(){
        GraphicsContext gc = body.getGraphicsContext2D();
        double[] bodyX = new double[]{187,124,99,97,120,133,169,181,194,211,214};
        double[] bodyY = new double[]{70,86,102,127,136,139,135,134,133,132,80};

        gc.setFill(darkColor);
        frontLeg1.draw(darkColor, darkColor);
        backLeg1.draw(darkColor, darkColor);
        gc.setFill(lightColor);
        gc.fillPolygon(bodyX, bodyY, bodyX.length);
        gc.strokePolyline(bodyX, bodyY, bodyX.length);
        frontLeg2.draw(darkColor, lightColor);
        backLeg2.draw(darkColor, lightColor);
    }

    private void drawTail(){
        GraphicsContext gc = body.getGraphicsContext2D();
        double[] x = new double[]{116,117,98,87,86,74,70,69,83,100};
        double[] y = new double[]{92,79,76,85,88,87,81,98,105,104};
        gc.setFill(darkColor);
        gc.fillPolygon(x,y,x.length);
        gc.strokePolyline(x,y,x.length);
    }

}
