package data.beastRider;

import data.AnimationOrder;
import data.Map.CompetitiveMap;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

import java.util.Random;

/**
 * Game Object that be competitor in game
 */
public class BeastRider extends Pane {
    /**
     * The animal that rider ride
     */
    private RideableAnimal pet;
    /**
     * Rider who ride the animal
     */
    private Rider rider;

    /**
     * The order of running animation
     */
    private AnimationOrder runningOrder;

    /**
     * Running speed
     */
    private double runSpeed = 5;

    /**
     * Maximum value of running speed
     */
    private final double maxRunSpeed = 100;

    /**
     * The maximum pixel that this object can be jump up while running
     */
    private final double maxJumpHeight = 10;

    /**
     * The pixel that this object can be jump up while running
     */
    private double jumpHeight = 0;
    /**
     * The boolean that check object is jump up or fall down while running
     */
    private boolean jumping = false;

    /**
     * The distance that rider is running pass
     */
    private double runDistance = 0;

    /**
     * Initialize object
     * Random running speed
     * Set scale to half
     * Set base running order to STOP
     * @param x position in X Axis
     * @param y position in Y Axis
     * @param pet the animal that rider ride
     */
    public BeastRider(int x, int y, RideableAnimal pet){
        setTranslateX(x);
        setTranslateY(y);
        setWidth(90);
        setHeight(145);
        setScaleX(0.5);
        setScaleY(0.5);
        runSpeed = new Random().nextInt(29)+3;

        runningOrder = AnimationOrder.STOP;

        setPetAndRider(pet);
    }

    /**
     * Jump up down slightly motion
     * Call animal's leg motion
     */
    public void runningMotion(){
        pet.legMotion();
        upDownMotion();
    }

    /**
     * Jump up down slightly motion
     */
    private void upDownMotion(){
        if (jumping){
            setTranslateY(getTranslateY()-0.1*runSpeed);
            jumpHeight += 0.1*runSpeed;
            if (jumpHeight >= maxJumpHeight) {
                jumping = false;
                setTranslateY(getTranslateY()+jumpHeight-maxJumpHeight);
                jumpHeight = maxJumpHeight;
            }
        }
        else {
            setTranslateY(getTranslateY()+0.1*runSpeed);
            jumpHeight -= 0.1*runSpeed;
            if (jumpHeight <= 0){
                jumping = true;
                setTranslateY(getTranslateY()+jumpHeight);
                jumpHeight = 0;
            }
        }
    }

    /**
     * Set the running speed and it won't be higher than 'maxRunSpeed' and lower than 1
     * @param newRunSpeed new running speed that use for assign to 'runSpeed'
     */
    public void setRunSpeed(double newRunSpeed){
        if (newRunSpeed >= maxRunSpeed)
            runSpeed = maxRunSpeed;
        else if (newRunSpeed <= 1)
            runSpeed = 1;
        else
            runSpeed = newRunSpeed;
        pet.setMovementSpeed(runSpeed);
    }

    /**
     * Changing X position by dSpeed
     * Increase runDistance by running speed
     * @param dSpeed changing speed
     */
    public void moveForward(double dSpeed){
        setTranslateX(getTranslateX()+dSpeed);
        if (runningOrder == AnimationOrder.START)
            runDistance+=runSpeed;
    }

    /**
     * Stop when runDistance over mapDistance
     * @param map the map that rider running in
     */
    public void finishCompetition(CompetitiveMap map){
        if (map.getMapPixelDistance() < runDistance){
            setRunningOrder(AnimationOrder.STOP);
        }
    }

    /**
     * Stop pet's running motion
     */
    public void stoppingPet(){
        pet.stopLegMotion();
    }

    /**
     * Get running speed
     * @return running speed
     */
    public double getRunSpeed() {
        return runSpeed;
    }

    /**
     * Get running order
     * @return running order
     */
    public AnimationOrder getRunningOrder() {
        return runningOrder;
    }

    /**
     * Set running order
     * @param runningOrder new AnimationOrder
     */
    public void setRunningOrder(AnimationOrder runningOrder) {
        this.runningOrder = runningOrder;
    }

    /**
     * Set rider's animal and new rider position refer by animal
     * @param pet new animal that set for rider
     */
    public void setPetAndRider(RideableAnimal pet) {
        this.pet = pet;
        this.pet.setMovementSpeed(runSpeed);
        if (pet instanceof Wolf){
            rider = new Rider(110,0);
        }
        else if (pet instanceof Horse){
            rider = new Rider(120,-10);
        }
        else if (pet instanceof Cat){
            rider = new Rider(106,-14);
        }
        getChildren().clear();
        getChildren().addAll(pet,rider);
    }

    /**
     * Changing rider's color and redraw
     * @param color the color of rider
     */
    public void setColorAndReDrawRider(Color color){
        rider.setColorAndReDraw(color);
    }
}
