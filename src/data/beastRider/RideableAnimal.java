package data.beastRider;

import javafx.scene.canvas.Canvas;
import javafx.scene.layout.Pane;

/**
 * The animal that rider ride
 */
abstract class RideableAnimal extends Pane{
    /**
     * Body part of animal
     */
    protected Canvas body;

    /**
     * Leg parts of animal
     */
    protected AnimalLeg frontLeg1, frontLeg2, backLeg1, backLeg2;

    /**
     * Initialize object
     */
    RideableAnimal() {
        setTranslateX(0);
        setTranslateY(0);
        setWidth(300);
        setHeight(200);

        body = new Canvas();
        body.setTranslateX(0);
        body.setTranslateY(0);
        body.setHeight(getHeight());
        body.setWidth(getWidth());
    }

    abstract void draw();

    /**
     * Calling each leg running motion
     */
    void legMotion(){
        frontLeg1.motion();
        frontLeg2.motion();
        backLeg1.motion();
        backLeg2.motion();
    }

    /**
     * Stopping running motion
     */
    void stopLegMotion(){
        frontLeg1.stopMotion();
        frontLeg2.stopMotion();
        backLeg1.stopMotion();
        backLeg2.stopMotion();
    }

    /**
     * set leg motion's angle
     * @param mspd the number that use for set leg's motion angle
     */
    void setMovementSpeed(double mspd){
        frontLeg1.setDAngle(mspd);
        frontLeg2.setDAngle(mspd);
        backLeg1.setDAngle(mspd);
        backLeg2.setDAngle(mspd);
    }

}
