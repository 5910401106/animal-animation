package data.beastRider;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

/**
 * The rider that cannot do anything except showing itself
 */
class Rider extends Canvas {

    /**
     * Rider color
     */
    Color mainColor;

    /**
     * Initialize Object and draw
     * @param x position in X Axis
     * @param y position in Y Axis
     */
    Rider(double x, double y){
        mainColor = Color.rgb(255, 188, 146);

        setTranslateX(x);
        setTranslateY(y);

        setWidth(90);
        setHeight(145);

        draw();
    }

    /**
     * drawing object
     */
    private void draw(){
        GraphicsContext gc = getGraphicsContext2D();
        gc.setLineWidth(5);
        gc.setStroke(mainColor);
        gc.setFill(mainColor);
        //draw
        gc.strokeOval(30,2,50,50);
        gc.fillOval(30,2,50,50);
        gc.strokeArc(6,35,113,125,120,60, ArcType.OPEN);
        gc.strokeArc(11,-4,151,82,210,50, ArcType.OPEN);
        gc.strokeArc(4,95,38,11,50,100, ArcType.OPEN);
        gc.strokeArc(-140,25,180,125,-50,40,ArcType.OPEN);
    }

    /**
     * set new color and redraw
     * @param color new color that assigned to mainColor
     */
    public void setColorAndReDraw(Color color){
        mainColor = color;
        getGraphicsContext2D().clearRect(0,0, getWidth(), getHeight());
        draw();
    }

}
