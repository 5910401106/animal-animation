# README #

This is Javafx animation project with theme Competition for subject 'Software Construction 01418217'  

created by Poonyapat Yanvisit 5910401106

### ABOUT ###
This project is about the rider competition.
They ride the animal and compete.
The winner is the one who complete the path first.

You are one of competitor who want to be the winner.
You have prepared for many years and this time you have to WIN!.

### OPTION ###

You can change your animal you want to ride with 3 type of animal

 * Wolf
 * Horse
 * Cat

And each animal type has 4 breed that only diffent by the color.

You can select how many rider who want to compete with (2 - 9 rider).

You can select how long you want to compete (choice is about 50 - 4000).

And You can select the map that you want to compete in.

 * Meadow Map
 * Castle Map
 * Forest Map
 * Warfield Map
 
### HOW TO PLAY ###

Press correct key to increase speed